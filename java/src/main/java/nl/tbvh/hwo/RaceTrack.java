package nl.tbvh.hwo;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;

public class RaceTrack {

    private final String name;
    private final String id;
    private final ArrayList<Piece> pieces = Lists.newArrayList();
    private double crachForce = 1e5;
    private double crachSpeed = 1e5;
    private final List<Double> craches = Lists.newLinkedList();
    private double deceleration;

    public RaceTrack(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public void addPiece(double length, boolean isSwitch) {
        pieces.add(new Piece(length, isSwitch));
    }

    public void addTurnPiece(double radius, double angle, boolean isSwitch) {
        pieces.add(new Piece(radius, angle, isSwitch));
    }

    private class Piece {

        private final boolean isSwitch;
        private final double length;
        private double radius;
        private double angle;
        private final int pieceIndex;
        public double speedLimit;

        public Piece(double length, boolean isSwitch, double speedLimit) {
            this.length = length;
            this.isSwitch = isSwitch;
            this.speedLimit = speedLimit;
            pieceIndex = pieces.size();

        }

        public Piece(double length, boolean isSwitch) {
            this(length, isSwitch, 1e5);
        }

        public Piece(double radius, double angle, boolean isSwitch) {
            this(Math.abs(angle / 360 * 2 * Math.PI * radius), isSwitch, 1e5);
            this.radius = radius;
            this.angle = angle;

        }

        public boolean isCorner() {
            return radius > 0;
        }

        public Piece nextPiece() {
            return RaceTrack.this.nextPiece(pieceIndex);
        }

        public double cornerLimit() {
            if (!isCorner()) {
                return 1e5;
            }
            return Math.sqrt(crachForce * radius) * .85;
        }

        private double distanceLeft(double inPieceDistance) {
            double distanceLeft = length - inPieceDistance;
            if (distanceLeft < 0) {
                // Outer lane in corner
                distanceLeft = 0;
            }
            return distanceLeft;
        }
    }

    public void updateCrachSpeed(Car car) {
        double radius = getPiece(car.getPosition()).radius;
        if (radius == 0) {
            // Crach on straight.. due to switch?
            return;
        }
        double thisCrachForce = car.getSpeed() * car.getSpeed() / radius;
        if (crachForce > 1e4) {
            crachForce = thisCrachForce * 1.2;
            crachSpeed = car.getSpeed();
            return;
            // Don't use this in the average.
        }
        craches.add(thisCrachForce);
        int n = craches.size();
        crachSpeed = (crachSpeed / n + car.getSpeed()) / (n + 1);
        crachForce = 0;
        for (Double force : craches) {
            crachForce += force;
        }
        crachForce /= n;
        crachForce *= 1.2 - n / 10.;
        // http://www.engineeringtoolbox.com/centripetal-acceleration-d_1285.html
    }

    public double getSpeed(double oldSpeed, Position position, Position newPosition) {
        if (position == null) {
            return oldSpeed;
        } else if (position.pieceIndex == newPosition.pieceIndex) {
            return newPosition.inPieceDistance - position.inPieceDistance;
        } else {
            return distanceLeft(position) + newPosition.inPieceDistance;
        }
    }

    private double distanceLeft(Position position) {
        Piece piece = getPiece(position);
        return piece.distanceLeft(position.inPieceDistance);
    }

    private Piece getPiece(Position position) {
        return pieces.get(position.pieceIndex);
    }

    public double getSpeedLimit(Car car) {
        Position position = car.getPosition();
        double limit = 1e5;// crachSpeed * 2;
        limit = Math.min(limit, getPiece(position).cornerLimit());
        limit = Math.min(limit, nextCornerLimit(car, position));
        return limit;
    }

    private double nextCornerLimit(Car car, Position position) {
        Piece nextPiece = nextPiece(position.pieceIndex);
        if (!nextPiece.isCorner() || car.getSpeed() < 1e-5) {
            return 1e5;
        }
        double distanceLeft = distanceLeft(position);
        double cornerLimit = nextPiece.cornerLimit();
        double tToCorner = distanceLeft / car.getSpeed();
        double decel = tToCorner * deceleration;
        return cornerLimit - decel;
    }

    private Piece nextPiece(int pieceIndex) {
        return pieces.get((pieceIndex + 1) % pieces.size());
    }

    public void setBreakDecel(double deceleration) {
        this.deceleration = -deceleration;
        System.out.println("Decelaration: " + this.deceleration);
    }

    public double getCrachForce() {
        return crachForce;
    }

    public boolean longStraight(Car car) {
        Position position = car.getPosition();
        Piece piece = getPiece(position);
        double distanceLeft = piece.distanceLeft(position.inPieceDistance);
        double neededDistance = crachSpeed * car.turboDurationMilliseconds / 60 * car.turboFactor * car.turboFactor;
        while (distanceLeft < neededDistance) {
            if (piece.isCorner()) {
                return false;
            }
            piece = piece.nextPiece();
            distanceLeft += piece.distanceLeft(0);
        }
        return true;
    }
}
