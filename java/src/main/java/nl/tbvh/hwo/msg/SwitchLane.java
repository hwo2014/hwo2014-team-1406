package nl.tbvh.hwo.msg;

public class SwitchLane extends Message {

    private final String direction;

    public SwitchLane(String direction) {
        super("switchLane");
        this.direction = direction;
    }

    @Override
    protected Object getData() {
        return direction;
    }

}
