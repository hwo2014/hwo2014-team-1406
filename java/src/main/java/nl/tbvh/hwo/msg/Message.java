package nl.tbvh.hwo.msg;

import com.google.gson.Gson;

public abstract class Message {
    private final String type;

    public Message(String type) {
        this.type = type;
    }

    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object getData() {
        return this;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        Object data = getData();
        if (data == this) {
            data = "[data]";
        }
        return getType() + ": " + data;
    }
}
