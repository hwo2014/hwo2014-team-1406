package nl.tbvh.hwo.msg;

public class MsgWrapper {
    public final String msgType;
    public final Object data;

    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final Message msg) {
        this(msg.getType(), msg.getData());
    }
}
