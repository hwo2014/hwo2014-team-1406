package nl.tbvh.hwo.msg;

public class Throttle extends Message {
    private final double value;

    public Throttle(double value) {
        super("throttle");
        if (value == Double.NaN) {
            IllegalArgumentException problem = new IllegalArgumentException("Cannot set throttle to NAN");
            // WTF hapend here...
            problem.printStackTrace();
            value = .4;
        }
        this.value = value;
    }

    @Override
    protected Object getData() {
        return value;
    }
}
