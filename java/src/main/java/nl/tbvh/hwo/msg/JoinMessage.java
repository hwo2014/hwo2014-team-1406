package nl.tbvh.hwo.msg;

public class JoinMessage extends Message {

    public final String name;
    public final String key;

    public JoinMessage(String name, String key) {
        super("join");
        this.name = name;
        this.key = key;
    }

}
