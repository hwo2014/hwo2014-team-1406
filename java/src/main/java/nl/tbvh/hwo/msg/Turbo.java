package nl.tbvh.hwo.msg;

public class Turbo extends Message {

    public Turbo() {
        super("turbo");
    }

    @Override
    protected Object getData() {
        return "Chaaaarge!";
    }

}
