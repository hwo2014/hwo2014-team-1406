package nl.tbvh.hwo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;

import nl.tbvh.hwo.msg.JoinMessage;
import nl.tbvh.hwo.msg.Message;
import nl.tbvh.hwo.msg.MsgWrapper;
import nl.tbvh.hwo.msg.Ping;
import nl.tbvh.hwo.msg.Throttle;

import org.json.JSONArray;
import org.json.JSONObject;

import com.google.gson.Gson;

public class RaceControl {
    private final Gson gson = new Gson();
    private final BufferedReader reader;
    private final PrintWriter writer;
    private RaceTrack track;
    private boolean gameStarted;
    private Car car;

    public RaceControl(BufferedReader reader, PrintWriter writer) {
        this.reader = reader;
        this.writer = writer;
    }

    private void join(String botName, String botKey) {
        send(new JoinMessage(botName, botKey));
    }

    public void race() {
        String line = null;
        while ((line = readLine()) != null) {
            MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            switch (msgFromServer.msgType) {
                case "gameStart":
                    System.out.println("Game STARTS!!!");
                    gameStarted = true;
                    send(new Throttle(1));
                    break;
                case "gameEnd":
                    System.out.println("Game is done...");
                    gameStarted = false;
                    break;
                case "yourCar":
                    JSONObject jCar = parseData(line);
                    car = new Car(jCar.getString("name"), jCar.getString("color"));
                    break;
                case "gameInit":
                    System.out.println("Got a gameInit");
                    parseRace(parseData(line));
                    break;
                case "carPositions":
                    raceTick(new JSONObject(line));
                    break;
                case "crash":
                    car.handleCrach(true, parseData(line));
                    break;
                case "spawn":
                    car.handleCrach(false, parseData(line));
                    break;
                case "turboAvailable":
                    System.out.println("Recieved turbo: " + line);
                    car.addTurbo(parseData(line));
                    break;
                case "join":
                    System.out.println("Joined the race server.");
                    break;
                case "turboEnd":
                    car.turboFactor = -1;
                    break;
                case "lapFinished":
                case "finish":
                case "tournamentEnd":
                    System.out.println(line);
                    break;
                default:
                    System.out.println("Unknown message: " + msgFromServer.msgType + "\n" + line);
                    send(new Ping());
                    break;
            }
        }
    }

    private JSONObject parseData(String line) {
        JSONObject jMsg = new JSONObject(line);
        return jMsg.getJSONObject("data");
    }

    private void raceTick(JSONObject jCarsUpdate) {
        JSONArray jCars = jCarsUpdate.getJSONArray("data");
        int gameTick = 0;
        if (jCarsUpdate.has("gameTick")) {
            gameTick = jCarsUpdate.getInt("gameTick");
        }
        for (int i = 0; i < jCars.length(); i++) {
            JSONObject jCarStatus = jCars.getJSONObject(i);
            JSONObject jCarId = jCarStatus.getJSONObject("id");
            if (car.is(jCarId)) {
                // This is us
                car.updatePosition(gameTick, jCarStatus);
            }
        }
        send(car.getInstruction());
    }

    private void parseRace(JSONObject jData) {
        if (track == null) {
            JSONObject jRace = jData.getJSONObject("race");
            track = new RaceTrackBuilder(jRace.getJSONObject("track")).build();
            car.setTrack(track);
        } else {
            System.out.println("Reset the car! Ready!");
            car.reset();
        }
        // parse cars
        // parse session
    }

    private void send(Message msg) {
        // System.out.println(msg);
        writer.println(msg.toJson());
        writer.flush();
    }

    private String readLine() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("Too bad..", e);
        }
    }

    public static void start(BufferedReader reader, PrintWriter writer, String botName, String botKey) {
        RaceControl raceControl = new RaceControl(reader, writer);
        startRace(raceControl, botName, botKey, null);
        // "keimola" "germany" "usa" "france"

        raceControl.race();
    }

    private static void startRace(RaceControl raceControl, String botName, String botKey, String trackName) {
        if (trackName == null) {
            raceControl.join(botName, botKey);
        } else {
            raceControl.writer.println("{\"msgType\":\"joinRace\",\"data\":{\"botId\":{\"name\":\"" + botName + "\",\"key\":\"" + botKey + "\"},\"trackName\":\"" + trackName
                    + "\",\"carCount\":1}}");
            raceControl.writer.flush();
        }
    }
}
