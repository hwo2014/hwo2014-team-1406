package nl.tbvh.hwo;

import org.json.JSONArray;
import org.json.JSONObject;

public class RaceTrackBuilder {

    private final JSONObject jTrack;
    private RaceTrack raceTrack;

    public RaceTrackBuilder(JSONObject jTrack) {
        this.jTrack = jTrack;
    }

    public RaceTrack build() {
        raceTrack = new RaceTrack(jTrack.getString("id"), jTrack.getString("name"));
        addPieces(jTrack.getJSONArray("pieces"));
        // addLanes();
        // addStartingPoint();
        return raceTrack;
    }

    private void addPieces(JSONArray jPieces) {
        for (int i = 0; i < jPieces.length(); i++) {
            addPiece(jPieces.getJSONObject(i));
        }
    }

    private void addPiece(JSONObject jPiece) {
        boolean isSwitch = false;
        if (jPiece.has("switch")) {
            isSwitch = jPiece.getBoolean("switch");
        }
        if (jPiece.has("radius")) {
            raceTrack.addTurnPiece(jPiece.getDouble("radius"), jPiece.getDouble("angle"), isSwitch);
        } else {
            raceTrack.addPiece(jPiece.getDouble("length"), isSwitch);
        }
    }

}
