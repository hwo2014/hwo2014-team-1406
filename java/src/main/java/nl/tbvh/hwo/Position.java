package nl.tbvh.hwo;

import org.json.JSONObject;

class Position {

    public final int pieceIndex;
    public final double inPieceDistance;
    public final int gameTick;
    private final int lap;

    public Position(JSONObject jPosition, int gameTick) {
        this.gameTick = gameTick;
        pieceIndex = jPosition.getInt("pieceIndex");
        inPieceDistance = jPosition.getDouble("inPieceDistance");
        lap = jPosition.getInt("lap");
        // "lane": {
        // "startLaneIndex": 1,
        // "endLaneIndex": 1
        // }
    }

}
