package nl.tbvh.hwo;

import nl.tbvh.hwo.msg.Message;
import nl.tbvh.hwo.msg.Ping;
import nl.tbvh.hwo.msg.SwitchLane;
import nl.tbvh.hwo.msg.Throttle;
import nl.tbvh.hwo.msg.Turbo;

import org.json.JSONObject;

public class Car {

    private static final double EPSILON = 1e-5;

    private final String color;
    private final String name;
    private boolean crached;

    double turboDurationMilliseconds;
    double turboDurationTicks;
    double turboFactor = -1;
    private RaceTrack track;
    private double angle;
    private Position position;
    private double speed;
    private double throttle;

    public Car(String name, String color) {
        this.name = name;
        this.color = color;
    }

    public boolean is(JSONObject jCarId) {
        return jCarId.getString("color").equals(color);
    }

    public void handleCrach(boolean crached, JSONObject jCrach) {
        if (is(jCrach)) {
            this.crached = crached;
            if (crached) {
                track.updateCrachSpeed(this);
                System.out.println("Crached with speed: " + speed + ", at: " + position.pieceIndex + ", tick: " + position.gameTick + ", new crachForce: " + track.getCrachForce());
                speed = 0;
            } else {
                System.out.println("Recovered from crach");
            }
        }
    }

    public void addTurbo(JSONObject jTurbo) {
        turboDurationMilliseconds = jTurbo.getDouble("turboDurationMilliseconds");
        turboDurationTicks = jTurbo.getDouble("turboDurationTicks");
        turboFactor = jTurbo.getDouble("turboFactor");
    }

    public void setTrack(RaceTrack track) {
        this.track = track;
    }

    public void updatePosition(int gameTick, JSONObject jCar) {
        angle = jCar.getDouble("angle");
        Position newPosition = new Position(jCar.getJSONObject("piecePosition"), gameTick);
        speed = track.getSpeed(speed, position, newPosition);
        position = newPosition;
    }

    public Message getInstruction() {
        if (crached) {
            return new Ping();
        }
        if (!breaksTested) {
            return breakTest();
        }
        if (turboFactor > 0 && track.longStraight(this)) {
            System.out.println("USING TURBO!!!");
            turboFactor = -1;
            return new Turbo();
        }
        double speedLimit = track.getSpeedLimit(this);
        double newThrottle = throttle;
        if (speedLimit * .97 > speed) {
            newThrottle = 1;
        } else if (speedLimit < speed) {
            newThrottle = 0;
        } else {
            newThrottle *= speedLimit / speed;
        }
        // FIXME: check the track, can we speed up?
        // otherwise maybe switch..
        if (noThrottleUpdate(newThrottle)) {
            double rnd = Math.random();
            if (rnd < .1) {
                return new SwitchLane("Left");
            } else if (rnd < .2) {
                return new SwitchLane("Right");
            }
        }
        return new Throttle(newThrottle);
    }

    // Break test
    private boolean breaksTested;

    private double breakSpeed;

    private Message breakTest() {
        int accelTicks = 25;
        int decelTicks = 5;

        if (position.gameTick < accelTicks) {
            return new Throttle(1);
        }
        if (position.gameTick == accelTicks) {
            breakSpeed = speed;
        }
        if (position.gameTick == accelTicks + decelTicks) {
            track.setBreakDecel((breakSpeed - speed) / decelTicks);
            breaksTested = true;
        }
        return new Throttle(0);
    }

    // Break test

    private boolean noThrottleUpdate(double newThrottle) {
        double diff = newThrottle - throttle;
        return -EPSILON < diff && diff < EPSILON;
    }

    public void reset() {
        crached = false;
        speed = 0;
        throttle = 0;
        position = null;
    }

    public double getSpeed() {
        return speed;
    }

    public Position getPosition() {
        return position;
    }

}
